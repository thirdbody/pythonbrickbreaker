# Milo Gilad
# 11/16/16
# Atari Breakout
from graphics import *
from tkinter import *
from random import randint
import graphics
import random
import time

def powerupMiniGame(powerupGame, powerupList, powerupChosen):
    win2 = GraphWin("Powerup Minigame", 800, 600) # New Window
    paddle = Rectangle(Point(350, 500), Point(445, 520)) # Making Paddle
    paddle.setFill("White") 
    paddle.draw(win2)
    randX = randint(400, 600) # Spawning point for powerup
    powerup = Circle(Point(randX, 185), 20)
    powerup.setFill("Orange")
    powerup.draw(win2)
    gameOn = True
    miniText = graphics.Text(Point(400, 300), "Minigame beginning in 5 seconds... Catch the ball with the paddle!")
    miniText.setSize(25)
    miniText.draw(win2)
    time.sleep(5)
    miniText.undraw()
    while gameOn is True:
        location = win2.winfo_pointerx() # Moving Paddle
        q = location - paddle.getCenter().getX()
        paddle.move(q, 0)
        powerup.move(0, 15)
        if paddle.getP1().getX() <= powerup.getCenter().getX() and paddle.getP2().getX() >= powerup.getCenter().getX():
            if paddle.getP2().getY() >= powerup.getCenter().getY() and paddle.getP1().getY() <= powerup.getCenter().getY():
                powerupText = graphics.Text(Point(400, 300), "Congrats! You got " + powerupList[powerupChosen] + "!")
                setpower = powerupChosen
                powerupText.setSize(33)
                powerupText.draw(win2)
                time.sleep(5)
                powerupText.undraw()
                gameOn = False
        elif powerup.getCenter().getY() >= 600:
            failText = graphics.Text(Point(400, 300), "Sorry, you didn't get the powerup.")
            failText.setSize(33)
            failText.draw(win2)
            time.sleep(5)
            failText.undraw()
            gameOn = False
    backText = graphics.Text(Point(400, 300), "Returning to regular gameplay in 3 seconds...")
    backText.setSize(33)
    backText.draw(win2)
    time.sleep(3)
    win2.close()

def main():
    win = GraphWin("The One and Only Neon Brick Breaker™ © Milo Gilad 2002-2102", 800, 600) # Setting Up (width, height)
    win.setBackground("Black")
    bricks = []
    colors = ['Green', 'Yellow', 'Red', 'Blue', 'Orange', 'Purple', 'Cyan', 'Green', 'Brown', 'Gray']
    x = 0
    y = 45
    for j in range(6):
        for i in range(10):
            x2 = x+80
            y2 = y+30
            rect = Rectangle(Point(x, y), Point(x2, y2))
            rect.setOutline(colors[randint(0, 9)])
            rect.draw(win)
            bricks.append(rect)
            x+=80
        x = 0
        y+=30
    paddle = Rectangle(Point(350, 500), Point(445, 520)) # Making Paddle
    paddle.setFill("White")
    paddle.draw(win)
    ball = Circle(Point(385, 485), 10) # Making Ball
    ball.setFill("White")
    ball.draw(win)
    ballX = 7
    ballY = -(randint(9, 14))
    lifeCount = randint(3, 7) # Making Life Counter
    lifeDisplay = graphics.Text(Point(100, 30), "Lives: " + str(lifeCount))
    lifeDisplay.setFill("White")
    lifeDisplay.setSize(36)
    lifeDisplay.draw(win)
    score = 0 # Making Score Counter
    scoreText = graphics.Text(Point(700, 30), "Score: " + str(score))
    scoreText.setSize(36)
    scoreText.setFill("White")
    scoreText.draw(win)
    powerupList = ["3-In-One", "Invinciblility", "+10 Score"] # Powerups
    doubleScore = 0
    superScore = 0
    powerupGame = False
    powerupChosen = 0
    mainGame = True
    while mainGame is True:
        location = win.winfo_pointerx() # Moving Paddle
        q = location - paddle.getCenter().getX()
        paddle.move(q, 0)
        ball.move(ballX, ballY) # Making Ball Bounce Around
        if ball.getCenter().getY() > 600:
            lifeDisplay.undraw()
            ball.undraw()
            ball.draw(win)
            ballY = -ballY
            lifeCount = lifeCount - 1
            lifeDisplay = graphics.Text(Point(100, 30), "Lives: " + str(lifeCount))
            lifeDisplay.setFill("White")
            lifeDisplay.setSize(36)
            lifeDisplay.draw(win)
        if ball.getCenter().getY() < 0:
            ballY = -ballY
        if ball.getCenter().getX() > 800:
            ballX = -ballX
        if ball.getCenter().getX() < 0:
            ballX = -ballX
        if ball.getCenter().getX() - 10 >= paddle.getP1().getX() and ball.getCenter().getX() + 10 <= paddle.getP2().getX() and ball.getCenter().getY() >= 485:
            ballY = -ballY
        if lifeCount <= 0:
            ball.undraw()
            youLose = graphics.Text(Point(400, 300), "You Lose.")
            youLose.setSize(33)
            youLose.setFill("White")
            youLose.draw(win)
            exit()
        if ball.getCenter().getY() <= 225:
            for i in bricks:
                if i.getP1().getX() <= ball.getCenter().getX() and i.getP2().getX() >= ball.getCenter().getX():
                    if i.getP2().getY() >= ball.getCenter().getY() and i.getP1().getY() <= ball.getCenter().getY():
                        score = score + 1
                        if powerupGame is True:
                            if powerupChosen is 0:
                                ball.setFill("Orange")
                                i.undraw()
                                bricks.remove(i)
                                doubleScore+=1
                                if doubleScore >= 3:
                                    powerupGame = False
                                    ball.setFill("White")
                                    doubleScore = 0
                            elif powerupChosen is 1:
                                ball.setFill("Orange")
                                i.undraw()
                                bricks.remove(i)
                                superScore += 1
                                if superScore >= 15:
                                    powerupGame = False
                                    ball.setFill("White")
                                    superScore = 0
                            elif powerupChosen is 2:
                                score+=10
                                powerupGame = False
                        else:
                                i.undraw()
                                ballY = -ballY
                                bricks.remove(i)
                                powerupGame = False
                        scoreText.undraw()
                        scoreText = graphics.Text(Point(700, 30), "Score: " + str(score))
                        scoreText.setSize(36)
                        scoreText.setFill("White")
                        scoreText.draw(win)
                        if len(bricks) == 0:
                            ball.undraw()
                            youWin = graphics.Text(Point(400, 300), "You Win!")
                            youWin.setSize(33)
                            youWin.setFill("White")
                            youWin.draw(win)
                            exit()
        if score is 10 or score is 20 or score is 30 or score is 40 or score is 50:
            randInt1 = randint(0, 2)
            if randInt1 is 2:
                if powerupGame is False:
                    powerupChosen = randint(0, 2) # Powerup defined as one of the three avaliable
                    mainGame = False
                    powerupMiniGame(powerupGame, powerupList, powerupChosen)
                    powerupGame = True
                    startText = graphics.Text(Point(400, 300), "Starting again in 4 seconds...")
                    startText.setFill("White")
                    startText.setSize(33)
                    startText.draw(win)
                    time.sleep(4)
                    startText.undraw()
                    mainGame = True
main()
